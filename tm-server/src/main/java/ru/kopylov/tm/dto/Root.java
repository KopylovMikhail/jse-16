package ru.kopylov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@XmlRootElement (name = "root")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Root {

    @NotNull
    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    private List<ProjectDto> projects = new ArrayList<>();

    @NotNull
    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    private List<TaskDto> tasks = new ArrayList<>();

}

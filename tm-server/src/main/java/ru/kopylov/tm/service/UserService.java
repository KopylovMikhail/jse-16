package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.IUserRepository;
import ru.kopylov.tm.api.service.IUserService;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.TypeRole;
import ru.kopylov.tm.repository.UserRepository;
import ru.kopylov.tm.util.HashUtil;
import ru.kopylov.tm.util.HibernateUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Collections;
import java.util.List;

@NoArgsConstructor
public final class UserService extends AbstractService implements IUserService {

    @Nullable
    private IUserRepository userRepository;

    @Nullable
    private EntityManagerFactory factory;

    @Nullable
    private EntityManager manager;

    public UserService(@NotNull ServiceLocator bootstrap) {
        super(bootstrap);
    }

    public boolean merge(@Nullable final User user) {
        if (user == null) return false;
        if (user.getId() == null || user.getId().isEmpty()) return false;
        if (user.getLogin() == null || user.getLogin().isEmpty()) return false;
        if (user.getPassword() == null || user.getPassword().isEmpty()) return false;
        factory = HibernateUtil.factory();
        manager = factory.createEntityManager();
        manager.getTransaction().begin();
        try {
            userRepository = new UserRepository(manager);
            userRepository.merge(user);
            manager.getTransaction().commit();
            return true;
        } catch (Exception e) {
            manager.getTransaction().rollback();
            e.printStackTrace();
            return false;
        } finally {
            manager.close();
            factory.close();
        }
    }

    public boolean merge(
            @NotNull final String userId,
            @NotNull final String login,
            @NotNull final String password
    ) {
        @NotNull final User user = findOne(userId); //new User();
        @NotNull final String hashPassword = HashUtil.hash(password);
        user.setLogin(login);
        user.setPassword(hashPassword);
        return merge(user);
    }

    public boolean persist(@Nullable final User user) {
        if (user == null) return false;
        factory = HibernateUtil.factory();
        manager = factory.createEntityManager();
        manager.getTransaction().begin();
        try {
            userRepository = new UserRepository(manager);
            userRepository.persist(user);
            manager.getTransaction().commit();
            return true;
        } catch (Exception e) {
            manager.getTransaction().rollback();
            e.printStackTrace();
            return false;
        } finally {
            manager.close();
            factory.close();
        }
    }

    public boolean persist(@NotNull final String login, @NotNull final String password) {
        if (password.isEmpty() || login.isEmpty()) return false;
        @NotNull final String hashPassword = HashUtil.hash(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPassword(hashPassword);
        user.setRole(TypeRole.USER);
        factory = HibernateUtil.factory();
        manager = factory.createEntityManager();
        manager.getTransaction().begin();
        try {
            userRepository = new UserRepository(manager);
            userRepository.persist(user);
            manager.getTransaction().commit();
            return true;
        } catch (Exception e) {
            manager.getTransaction().rollback();
            e.printStackTrace();
            return false;
        } finally {
            manager.close();
            factory.close();
        }
    }

    @Nullable
    public User findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        factory = HibernateUtil.factory();
        manager = factory.createEntityManager();
        manager.getTransaction().begin();
        try {
            userRepository = new UserRepository(manager);
            @Nullable final User user = userRepository.findOne(id);
            manager.getTransaction().commit();
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            manager.close();
            factory.close();
        }
    }

    @Nullable
    public User findOne(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        @NotNull final String hashPassword = HashUtil.hash(password);
        factory = HibernateUtil.factory();
        manager = factory.createEntityManager();
        manager.getTransaction().begin();
        try {
            userRepository = new UserRepository(manager);
            @Nullable final User user = userRepository.findOne(login, hashPassword);
            manager.getTransaction().commit();
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            manager.close();
            factory.close();
        }
    }

    @NotNull
    public List<User> findAll() {
        factory = HibernateUtil.factory();
        manager = factory.createEntityManager();
        manager.getTransaction().begin();
        try {
            userRepository = new UserRepository(manager);
            @Nullable final List<User> users = userRepository.findAll();
            manager.getTransaction().commit();
            return users;
        } catch (Exception e) {
//            manager.getTransaction().rollback();
            e.printStackTrace();
            return Collections.emptyList();
        } finally {
            manager.close();
            factory.close();
        }
    }

    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        factory = HibernateUtil.factory();
        manager = factory.createEntityManager();
        manager.getTransaction().begin();
        try {
            userRepository = new UserRepository(manager);
            userRepository.remove(id);
            manager.getTransaction().commit();
        } catch (Exception e) {
            manager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            manager.close();
            factory.close();
        }
    }

}

package ru.kopylov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.entity.AbstractEntity;

import javax.persistence.EntityManager;

@NoArgsConstructor
public abstract class AbstractRepository<E extends AbstractEntity> {

    @NotNull
    protected EntityManager manager;

    protected AbstractRepository(@NotNull final EntityManager manager) {
        this.manager = manager;
    }

    public void persist(@NotNull final E entity) {
        manager.persist(entity);
    }

    public void merge(@NotNull final E entity) {
        manager.merge(entity);
    }

    public abstract void remove(@NotNull final String id);

    public abstract E findOne(@NotNull final String id);

}

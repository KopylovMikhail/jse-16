package ru.kopylov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.entity.Session;

public interface ISessionRepository {

    void persist(@NotNull Session session);

    void merge(@NotNull Session session);

    void remove(@NotNull String id);

    @NotNull
    Session findOne(@NotNull String id);

    void removeByUserId(@NotNull String userId);

}

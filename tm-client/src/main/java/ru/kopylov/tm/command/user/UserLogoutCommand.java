package ru.kopylov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class UserLogoutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "logout";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Complete user session.";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getUserEndpoint().logoutUser(bootstrap.getToken());
        System.out.println("[LOGOUT SUCCESS]\n");
    }

}

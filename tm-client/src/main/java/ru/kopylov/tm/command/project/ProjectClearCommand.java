package ru.kopylov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getProjectEndpoint().clearProject(bootstrap.getToken());
        System.out.println("[ALL PROJECTS REMOVED]\n");
    }

}

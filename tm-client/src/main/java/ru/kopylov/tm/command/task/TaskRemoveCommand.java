package ru.kopylov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.endpoint.TaskDto;
import ru.kopylov.tm.util.CommandUtil;

import java.util.List;

@NoArgsConstructor
public final class TaskRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK REMOVE]");
        @NotNull final List<TaskDto> tasks = bootstrap.getTaskEndpoint().getTaskList(bootstrap.getToken(), null);
        CommandUtil.printTaskListWithParam(tasks);
        System.out.println("ENTER EXISTING TASK NUMBER:");
        @Nullable final String terminalCommand = bootstrap.getTerminalService().getReadLine();
        if (terminalCommand == null || terminalCommand.isEmpty()) {
            System.out.println("Number is empty.");
            return;
        }
        @NotNull final Integer taskNumber = Integer.parseInt(terminalCommand);
        final boolean removeSuccess = bootstrap.getTaskEndpoint().removeTask(bootstrap.getToken(), taskNumber);
        if (removeSuccess)
            System.out.println("[TASK REMOVED]\n");
        else System.out.println("Such a task does not exist or name is empty.");
    }

}

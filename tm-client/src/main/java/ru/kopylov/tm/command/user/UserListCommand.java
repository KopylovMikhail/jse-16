package ru.kopylov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.endpoint.UserDto;

import java.util.List;

public class UserListCommand extends AbstractCommand {

    @Override
    public @NotNull String getName() {
        return "user-list";
    }

    @Override
    public @NotNull String getDescription() {
        return "Show all users. Only for administrator.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER LIST]");
        @NotNull final List<UserDto> users = bootstrap.getUserEndpoint().getUserList(bootstrap.getToken());
        for (int i = 0; i < users.size(); i++) {
            System.out.println(i+1 + ". " + users.get(i).getLogin() + ", " + users.get(i).getRole() + ", " + users.get(i).getId());
        }
    }

}
